from producer_parser import ProducerParser
from bs4 import BeautifulSoup
from jinja2 import Template


class Iiyama(ProducerParser):

    def _load_data(self, content):
        device_name = ''
        desc = ''
        features = []
        specs = {}

        soup = BeautifulSoup(content, 'html.parser')

        device_name = soup.find("div", attrs={"class": "leadtxt"}).find('h3').get_text()
        if device_name is not None:
            device_name = device_name.replace("\n", "").replace("\t", "")
        else:
            device_name = ''
        desc = soup.find(attrs={"class": "x-description"}).get_text()

        if device_name is not None:
            self.filename = device_name

        features_div = soup.find(id='featureslider')
        if features_div is None:
            features_div = soup.find(id="features")

        if features_div is not None:
            features_list = []
            features_list_div = features_div.find(attrs={"class": "slides"})
            if features_list_div is None:
                features_list = features_div.find("div", attrs={"class": "imageFeatures"}).find_all("div", attrs={
                    "class": "row"})
            else:
                features_list = features_list_div.children
            for feature in features_list:
                if feature != "\n" and feature is not None:
                    feature_name = feature.find('h4')
                    if feature_name is None:
                        feature_name = feature.find('h3')
                        if feature_name is None:
                            feature_name = ""

                    if feature_name != "":
                        feature_name = feature_name.get_text()
                    feature_text = feature.find('p').get_text()
                    feature_img_div = feature.find('img')
                    feature_img = feature_img_div['src']
                    if feature_img is not None:
                        feature_img = "https:" + feature_img
                    features.append({"name": feature_name, "text": feature_text, "img": feature_img})

        specification_divs = soup.find_all(attrs={'class': 'speci'})
        if specification_divs is not None and specification_divs != []:
            for specification_section in specification_divs:
                spec_name = specification_section.find('h4').a.get_text()
                rows = specification_section.find_all('tr')
                rows_map = {}
                for row in rows:
                    row_key = row.find(attrs={'align': 'right'}).strong.get_text()
                    row_value = row.find_all('td')[1]
                    row_value = row_value.get_text()
                    if row_value is not None:
                        row_value = row_value.replace("\t", "").replace("\n", "")
                    else:
                        row_value = ""
                    rows_map[row_key] = row_value
                specs[spec_name] = rows_map

        if device_name is None:
            device_name = ""
        if desc is None:
            desc = ''
        if features is None:
            features = []
        if specs is None:
            specs = {}

        data_map = {
            "name": device_name,
            "desc": desc,
            "features": features,
            "specs": specs
        }
        return data_map


    def _generate(self, data):
        # read template html
        template_file = open("iiyama.html", "r")
        template_html = template_file.read()
        template_file.close()

        template = Template(template_html)
        html = template.render(name=data['name'], desc=data['desc'], features=data['features'], specs=data['specs'])

        return html
