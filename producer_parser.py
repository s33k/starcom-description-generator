import subprocess
from tkinter import *

import os
import requests
import json


class ProducerParser:
    link = ''
    filename = ''
    path = ''
    window = ''
    force = False

    def __init__(self, link, path='', force=False):
        self.link = link
        self.path = path
        self.force = force

    def _generate(self, data):
        return "test"

    def _load_data(self, content):
        return {}

    def _save(self, html):
        filename = self.filename + ".html"
        path = os.path.join(self.path, filename)
        file = open(path, "w", encoding='utf-8')
        file.write(html)
        file.close()

    def _submit(self, data={}):
        if not self.force:
            json_file = open(self.filename + ".json", "r", encoding='utf8')
            data_text = json_file.read()
            json_file.close()
            data = json.loads(data_text)

        html = self._generate(data)
        self._save(html)

        if not self.force:
            self.window.destroy()
            os.remove(self.filename + ".json")

    def _edit_data(self, data):
        json_text = json.dumps(data)
        edit_file = open((self.filename + ".json"), "w", encoding='utf8')
        edit_file.write(json_text)
        edit_file.close()

        self.window = Tk()
        self.window.title("Pobrane dane")
        label = Label(self.window, text=self.filename)
        label.pack(side=TOP)
        open_btn = Button(self.window, text="Otwórz plik do edycji", command=lambda: os.system("code " + "\"" + self.filename + ".json" + "\""))
        open_btn.pack(side=LEFT, pady=10)
        btn = Button(self.window, command=lambda: self._submit(), text="Zapisz")
        btn.pack(side=LEFT, fill=X)
        self.window.mainloop()

    def load(self,):
        response = requests.get(self.link)
        data = self._load_data(response.content)
        if self.force:
            self._submit(data)
        else:
            self._edit_data(data)
