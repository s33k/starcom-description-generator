from tkinter import *
from tkinter import filedialog
from tkinter import messagebox

from Epson import Epson
from brother import Brother
from iiyama import Iiyama


def generate():
    scraper = ''
    if 'iiyama.com' in link_var.get():
        scraper = Iiyama(link_field.get(), path=path_var.get(), force=force_var.get())
    elif 'brother.pl/' in link_var.get():
        scraper = Brother(link_field.get(), path=path_var.get(), force=force_var.get())
    elif 'epson.pl' in link_var.get():
        scraper = Epson(link_field.get(), path=path_var.get(), force=force_var.get())

    if scraper is not None:
        try:
            scraper.load()
        except Exception as e:
            messagebox.showerror(title="Error", message=e)
    else:
        messagebox.showinfo(title="Error", message="Podaj stronę producenta. Epson, iiyama albo brother")


def select_path():
    path = filedialog.askdirectory()
    path_var.set(path)


window = Tk()
window.title("Generator opisów")
window.grid_columnconfigure(0, weight=1)

link_label = Label(window, text="Link do oficjalnej strony produktu")
link_label.grid(column=0, row=1, columnspan=2, sticky=W, pady=15)

link_var = StringVar()

link_field = Entry(window, width=30, textvariable=link_var)
link_field.grid(column=2, row=1, columnspan=2, sticky=W)

force_var = BooleanVar(value=True)

force_btn = Checkbutton(window, var=force_var, text="Bez poprawek")
force_btn.grid(column=0, row=3, sticky=W)

path_var = StringVar()

path_label = Label(window, text="", textvariable=path_var, wrap=True, wraplength=200)
path_label.grid(column=2, row=2)

btn_path = Button(window, text="Wybierz ścieżkę do zapisu", command=select_path)
btn_path.grid(column=0, row=2, pady=5, sticky=W)

btn = Button(window, text="Generuj stronę", command=generate, width=58)
btn.grid(column=0, row=6, columnspan=4, pady=5, )

window.mainloop()