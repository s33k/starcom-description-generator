from jinja2 import Template
from producer_parser import ProducerParser
from lxml import etree
from lxml.html.clean import unicode


class Epson(ProducerParser):

    def _generate(self, data):
        template_file = open("epson.html", "r")
        template_html = template_file.read()
        template_file.close()

        template = Template(template_html)
        html = template.render(name=data['name'], short_desc=data['short_desc'], desc=data['desc'], features=data['key_features'], specs=data['specs'])

        return html

    def _load_data(self, content):
        parser = etree.HTMLParser()
        content = unicode(content, 'utf-8')
        content = content.replace("<sup>1</sup>", "")
        content = content.replace("<sup>2</sup>", "")
        content = content.replace("<sup>3</sup>", "")
        content = content.replace("<sup>4</sup>", "")
        content = content.replace("<sup>5</sup>", "")
        content = content.replace("<sup>", "")
        content = content.replace("</sup>", "")

        page_html = etree.fromstring(content, parser=parser)

        name = page_html.xpath('//h1[@itemprop="name"]/text()')[0]
        printer_name = name
        self.filename = name

        short_description = page_html.xpath('//p[@class="headline"]/text()')
        if short_description is not None:
            short_description = short_description[0]
        else:
            short_description = ""

        descriptions = page_html.xpath('//div[@class="columns s-sixteen l-eight six"]')
        descriptions += page_html.xpath('//div[@class="columns s-sixteen eight offset-by-two"]')
        descriptions_map = {}
        for desc in descriptions:
            children = desc.getchildren()
            if len(children) == 2:
                desc_name = children[0].getchildren()[0].text
                desc_detail = children[1].text
                descriptions_map[desc_name] = desc_detail

        key_feature = page_html.xpath('//div[@class="key-features"]')[0].getchildren()[0].getchildren()[1].getchildren()
        keys = {}
        for index, key in enumerate(key_feature):
            if ":" in key.text:
                text = key.text.split(':')
                keys[text[0]] = text[1]
            else:
                text = key.text
                keys[index] = text

        tech_data_divs = page_html.xpath('//ul[@class="collapsible-list"]')
        if tech_data_divs is not None:
            tech_data_divs = tech_data_divs[0]
        tech_data = {}
        for data in tech_data_divs:
            data_name = data.getchildren()[0].text
            data_map = {}
            div = data.getchildren()[1].getchildren()[0].getchildren()
            for row in div:
                try:
                    row_children = row.getchildren()
                    row_head = row_children[0].getchildren()[0].text
                    row_body = row_children[1].text
                    data_map[row_head] = row_body
                except Exception as e:
                    print(e)
            tech_data[data_name] = data_map



        data_map = {
            "name": self.filename,
            "short_desc": short_description,
            "desc": descriptions_map,
            "key_features": keys,
            "specs": tech_data,
        }

        return data_map

