from jinja2 import Template
from producer_parser import ProducerParser
from bs4 import BeautifulSoup


class Brother(ProducerParser):

    def _generate(self, data):
        template_file = open("brother.html", "r")
        template_html = template_file.read()
        template_file.close()

        template = Template(template_html)
        html = template.render(name=data['name'], short_desc=data['short_desc'], desc=data['desc'], features=data['key_features'], specs=data['specs'], comp_with=data["comp_with"])

        return html

    def _load_data(self, content):
        soup = BeautifulSoup(content, 'html.parser')

        product_name = soup.find('h1', attrs={'itemprop': 'name'})
        self.filename = product_name.get_text()

        key_features_parent = soup.find('section', attrs={'class': "product-detail--key-features"})
        key_features = key_features_parent.find_all('li')
        keys = []
        for key in key_features:
            keys.append(key.get_text())

        description_parent = soup.find(id='overview')
        if description_parent is None:
            description_parent = soup.find(id='Overview')
        if description_parent is None:
            description_parent = soup.find(id='áttekintés')
        if description_parent is None:
            description_parent = soup.find(id='Áttekintés')

        description_div = description_parent.find(attrs={'class': 'col-md-8'})
        short_description = description_div.get_text()

        description = ""
        for child in description_div.children:
            description += child.__str__()

        specifications = {}
        compatible_with = []

        main_specifications_parent = soup.find(id='specifications')
        if main_specifications_parent is None:
            main_specifications_parent = soup.find(id="Specifications")
        if main_specifications_parent is None:
            main_specifications_parent = soup.find(id="specification-details")
        if main_specifications_parent is None:
            main_specifications_parent = soup.find(id="specification")
        if main_specifications_parent is None:
            main_specifications_parent = soup.find(id="specifikáció")
        if main_specifications_parent:
            specifications_sections = main_specifications_parent.find_all(attrs={'class': "common-accordion--item"})
            for specs in specifications_sections:
                specs_section_title = specs.find('h4').get_text()
                trs = specs.find_all("tr")
                rows = {}
                for tr in trs:
                    th = tr.find('th').get_text()
                    td = tr.find('td').get_text()
                    rows[th] = td
                specifications[specs_section_title] = rows
        else:
            supplies_main_parent = soup.find(id='supplies')
            if supplies_main_parent is None:
                supplies_main_parent = soup.find(id='kellékek')
            lis = supplies_main_parent.find_all('li')
            for li in lis:
                device_name = li.find('h4').get_text()
                compatible_with.append(device_name)

        if short_description is None:
            short_description = ""
        if description is None:
            description = ""
        if specifications is None:
            specifications = {}
        if compatible_with is None:
            compatible_with = []

        data_map = {
            "name": self.filename,
            "short_desc": short_description,
            "desc": description,
            "key_features": keys,
            "specs": specifications,
            "comp_with": compatible_with
        }

        return data_map

